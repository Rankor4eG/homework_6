
const btn = document.querySelector('.btn')
const ipContainer = document.querySelector('.ip')

const urlIp = 'https://api.ipify.org/?format=json';
const urlIpInfo = 'http://ip-api.com/json/'

async function request(url){
    let response = await fetch(url);
    return await response.json();
    
}
   

    class Data {
        constructor({countryCode, country, region, city, regionName,query}){
            this.countryCode = countryCode;
            this.country = country;
            this.region = region;
            this.city = city;
            this.regionName = regionName;
            this.query = query;
        }    

        renderInfo(){
            const info = document.createElement('div');
            info.classList.add('card');
            
            info.insertAdjacentHTML('afterbegin', `
            
                <div class='card__info'>
                    <h2 class='card__title'>IP:</h2>
                    <p  class='card__text'>${this.query}</p>
                </div>

                <div class='card__info'>
                    <h2 class='card__title'>Country/countryCode:</h2>
                    <p  class='card__text'>${this.country}/${this.countryCode}</p>
                </div>

                <div class='card__info'>
                    <h2 class='card__title'>City/Region:</h2>
                    <p  class='card__text'>${this.city}/${this.regionName}</p>
                </div>
                
                <div class='card__info'>
                    <h2 class='card__title'>Region:</h2>
                    <p  class='card__text'>${this.region}</p>
                </div>
            `)
            ipContainer.insertAdjacentElement('afterend', info)
        } 
    }

btn.addEventListener('click', async()=>{
    const ipInfo = await request(urlIp)
    const data = await request(`http://ip-api.com/json/${ipInfo.ip}?fields=status,message,country,countryCode,region,regionName,city,zip,lat,lon,timezone,isp,org,as,query`);
    new Data(data).renderInfo()
})
    
    

                
                
                
                
                